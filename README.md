# Mybatis逆向工程

#### 介绍
生成pojo和mapper，包括对象的简单操作模板类

#### 软件架构
软件架构说明


#### 安装教程

1. 拉取软件之后添加lib下的jar包（里面必须包含数据库驱动包如果没有，请自己另外下载）
2. 在配置文件generatorConfig.xml中配置数据库和表名称
3. 在类GeneratorSqlmap.java中运行主方法即可

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)